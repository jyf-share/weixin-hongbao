import { Component } from '@angular/core';
import { trigger,state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('openHongbaoAnm', [
      state('inactive', style({ transform: 'rotateY(0)' })), // 无动作状态
      state('active', style({ transform: 'rotateY(360deg)' })), // 有动作状态(翻转360度)
      transition('inactive => active', animate('0.8s ease-out')) // 无动作到有动作的动画时间以及过渡方式
    ])
  ]
})
export class AppComponent {
  public loading = false;  // 数据加载loading
  public openState = 'inactive';  // 动画状态

  // 开红包动画完成事件
  public openHongbaoAnmDone(e) {
    /**
     * 说明: 当前状态为inactive时, 若数据数据还处于loading, 则切换至有动画的active状态
     * active状态完成后切换回无状态inactive,这时候又会进入下一轮inactive判断
     * 视为无限播放动画, 直到有数据返回时, 即loading为true, 停止动画
     */
    switch (e.toState) {
      case 'inactive':
        return this.openState = (this.loading) ? 'active' : 'inactive';
      case 'active':
        return this.openState = 'inactive';
    }
  }

  // 开红包
  public openHongbao() {
    this.loading = true;
    this.openState = 'active'  // 开始动画

    // 2秒后停止动画
    setTimeout(() => {
      this.loading = false;
    }, 2000);
  }

}
