1. 安装依赖
  ```
  $ npm config set registry https://registry.npm.taobao.org
  $ npm i
  ```

2. 启动应用
  ```
  $ ng serve
  ```

3. 查看应用
  在浏览器打开 http://localhost:4200